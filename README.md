# Generated Documentation
## test_workflow2
Description: your role description


| Field                | Value           |
|--------------------- |-----------------|
| Functional description | Not available. |
| Requester            | Not available. |
| Users                | Not available. |
| Date dev             | Not available. |
| Date prod            | Not available. |
| Readme update            | 02/10/2023 |
| Version              | Not available. |
| Time Saving              | Not available. |
| Category              | Not available. |
| Sub category              | Not available. |


### Defaults
**These are static variables with lower priority**
#### File: main.yml
| Var          | Type         | Value       | Required    | Title       |
|--------------|--------------|-------------|-------------|-------------|
| variable_test    | str   | test1  | True  | Variable test |
| variable_workflow    | str   | workflow1  | True  | Variable workflow |
| autre_var    | str   | value1  | True  | Variable autre |
| simple_var    | str   | value2  | True  | Variable simple |



### Vars

No vars available.


### Tasks
| Name | Module | Condition |
| ---- | ------ | --------- |
| Début du workflow complexe | ansible.builtin.debug | False |
| Configurer une variable dynamiquement | set_fact | False |
| Afficher dynamic_var | ansible.builtin.debug | False |
| Block 1 - Début | block | False |
| Block 1 - Tâche 1 | ansible.builtin.debug | True |
| Block 1 - Tâche 2 | ansible.builtin.debug | True |
| Block 2 - Début | block | False |
| Block 2 - Tâche 1 | ansible.builtin.debug | True |
| Block 2 - Tâche 2 | ansible.builtin.debug | True |
| Block 3 - Début | block | False |
| Block 3 - Tâche 1 | ansible.builtin.debug | True |
| Fin du workflow complexe | ansible.builtin.debug | False |


## Task Flow Graphs

### Graph for main.yml
```mermaid
flowchart TD
  Start
  Start-->|Task| Début_du_workflow_complexe[début du workflow complexe]
  Début_du_workflow_complexe-.->|End of Task| Début_du_workflow_complexe
  Début_du_workflow_complexe-->|Task| Configurer_une_variable_dynamiquement[configurer une variable dynamiquement]
  Configurer_une_variable_dynamiquement-.->|End of Task| Configurer_une_variable_dynamiquement
  Configurer_une_variable_dynamiquement-->|Task| Afficher_dynamic_var[afficher dynamic var]
  Afficher_dynamic_var-.->|End of Task| Afficher_dynamic_var
  Afficher_dynamic_var-->|Block Start| Block_1___Début_block_start_0[block 1   début]
  Block_1___Début_block_start_0-->|Task| Block_1___Tâche_1_when_variable_test_____test1_[block 1   tâche 1]
  Block_1___Tâche_1_when_variable_test_____test1_---|When: variable test     test1 | Block_1___Tâche_1_when_variable_test_____test1_
  Block_1___Tâche_1_when_variable_test_____test1_-->|Task| Block_1___Tâche_2_when_variable_test_____test1_[block 1   tâche 2]
  Block_1___Tâche_2_when_variable_test_____test1_---|When: variable test     test1 | Block_1___Tâche_2_when_variable_test_____test1_
  Block_1___Tâche_2_when_variable_test_____test1_-.->|End of Block| Block_1___Début_block_start_0
  Block_1___Tâche_2_when_variable_test_____test1_-->|Block Start| Block_2___Début_block_start_0[block 2   début]
  Block_2___Début_block_start_0-->|Task| Block_2___Tâche_1_when_variable_workflow_____workflow1__and_autre_var_____value1_[block 2   tâche 1]
  Block_2___Tâche_1_when_variable_workflow_____workflow1__and_autre_var_____value1_---|When: variable workflow     workflow1  and autre var    <br>value1 | Block_2___Tâche_1_when_variable_workflow_____workflow1__and_autre_var_____value1_
  Block_2___Tâche_1_when_variable_workflow_____workflow1__and_autre_var_____value1_-->|Task| Block_2___Tâche_2_when_variable_workflow_____workflow1__and_autre_var_____value1_[block 2   tâche 2]
  Block_2___Tâche_2_when_variable_workflow_____workflow1__and_autre_var_____value1_---|When: variable workflow     workflow1  and autre var    <br>value1 | Block_2___Tâche_2_when_variable_workflow_____workflow1__and_autre_var_____value1_
  Block_2___Tâche_2_when_variable_workflow_____workflow1__and_autre_var_____value1_-->|Block Start| Block_3___Début_block_start_1[block 3   début]
  Block_3___Début_block_start_1-->|Task| Block_3___Tâche_1_when_simple_var_____value2_[block 3   tâche 1]
  Block_3___Tâche_1_when_simple_var_____value2_---|When: simple var     value2 | Block_3___Tâche_1_when_simple_var_____value2_
  Block_3___Tâche_1_when_simple_var_____value2_-.->|End of Block| Block_3___Début_block_start_1
  Block_3___Tâche_1_when_simple_var_____value2_-.->|End of Block| Block_2___Début_block_start_0
  Block_3___Tâche_1_when_simple_var_____value2_-->|Rescue Start| Block_2___Début_rescue_start_0[block 2   début]
  Block_2___Début_rescue_start_0-->|Task| Une_erreur_s_est_produite_dans_Block_2[une erreur s est produite dans block 2]
  Une_erreur_s_est_produite_dans_Block_2-.->|End of Rescue Block| Block_2___Début_block_start_0
  Une_erreur_s_est_produite_dans_Block_2-->|Task| Fin_du_workflow_complexe[fin du workflow complexe]
  Fin_du_workflow_complexe-.->|End of Task| Fin_du_workflow_complexe
```


## Playbook
```yml
---
- hosts: localhost
  remote_user: root
  roles:
    - test_workflow2

```
## Playbook graph
```mermaid
flowchart TD
  localhost-->|Role| test_workflow2[test workflow2]
```

## Author Information
your name

#### License
license (GPL-2.0-or-later, MIT, etc)

#### Minimum Ansible Version
2.1

#### Platforms
No platforms specified.